// globals because things in setup() can only be used there
xphrase = "arsenalfc3";
yphrase = "1manunited";

// 30 is just a nice size in the browser
// if changed will need to change the hardcoded text positioning pixel values
// and probably the text size and/or stroke weight
interval = 30;

function setup() {
	createCanvas(
		(xphrase.length + 3) * interval,
		(yphrase.length + 3) * interval
	);
	background("#263353");

	drawingContext.setLineDash([0, 0]);
	textSize(18);

	strokeWeight(2);
	fill("#263353");
	stroke(255);
	rect(
		width / (xphrase.length + 3),
		height / (yphrase.length + 3),
		width - interval * 2,
		height - interval * 2
	);

	fill(255);

	drawingContext.setLineDash([interval, interval]);
}

function draw() {
	// have to be set here instead of in globals
	xstep = 0;
	ystep = 0;

	for (var x = interval * 2; x < width - interval; x += interval) {
		stroke(255);
		// console.log(xstep, xphrase[xstep], isVowel(xphrase[xstep]));
		if (isVowel(xphrase[xstep])) {
			// enable for color
			// stroke(255,0,0);
			line(x, interval * 2, x, height - interval);
			// enable after invoking stroke() to keep text elements as they are
			noStroke();
			text(xphrase[xstep].toUpperCase(), x - 6, 24);
		} else {
			// stroke(255,0,0);
			line(x, interval, x, height - interval);
			noStroke();
			text(xphrase[xstep].toUpperCase(), x - 6, 24);
		}
		// stop one short to keep the string inside the rectangle
		if (xstep < xphrase.length - 1) {
			xstep++;
		}
	}

	for (var y = interval * 2; y < height - interval; y += interval) {
		stroke(255);
		// console.log(ystep, yphrase[ystep], isVowel(yphrase[ystep]));
		if (isVowel(yphrase[ystep])) {
			// stroke(0);
			line(interval * 2, y, width - interval, y);
			noStroke();
			text(yphrase[ystep].toUpperCase(), width - 24, y + 6);
		} else {
			// stroke(0);
			line(interval, y, width - interval, y);
			noStroke();
			text(yphrase[ystep].toUpperCase(), width - 24, y + 6);
		}
		// stop one short to keep the string inside the rectangle
		if (ystep < yphrase.length - 1) {
			ystep++;
		}
	}
}

// vowels and odd numbers start off/away from the border
function isVowel(c) {
	var result;
	if (int(c)) {
		if (int(c) % 2 != 0) {
			result = true;
		}
	} else {
		c = c.toUpperCase();
		result = c == "A" || c == "E" || c == "I" || c == "O" || c == "U";
	}
	return result;
}
