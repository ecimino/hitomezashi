
let xphrase = "";
let yphrase = "";
const interval = 30;

function setup() {
    createCanvas(
        (xphrase.length + 3) * interval,
        (yphrase.length + 3) * interval
    );
    background("#263353");
    drawRect();
}

function draw() {
    drawGrid();
}

function drawRect() {
    drawingContext.setLineDash([0, 0]);
    textSize(18);
    strokeWeight(2);
    fill("#263353");
    stroke(255);
    rect(
        width / (xphrase.length + 3),
        height / (yphrase.length + 3),
        width - interval * 2,
        height - interval * 2
    );
    fill(255);
    drawingContext.setLineDash([interval, interval]);
}

function drawGrid() {
    let xstep = 0, ystep = 0;

    for (let x = interval * 2; x < width - interval; x += interval) {
        stroke(255);
        if (isVowel(xphrase[xstep])) {
            line(x, interval * 2, x, height - interval);
            noStroke();
            text(xphrase[xstep].toUpperCase(), x - 6, 24);
        } else {
            line(x, interval, x, height - interval);
            noStroke();
            text(xphrase[xstep].toUpperCase(), x - 6, 24);
        }
        if (xstep < xphrase.length - 1) xstep++;
    }

    for (let y = interval * 2; y < height - interval; y += interval) {
        stroke(255);
        if (isVowel(yphrase[ystep])) {
            line(interval * 2, y, width - interval, y);
            noStroke();
            text(yphrase[ystep].toUpperCase(), 6, y + 6);
        } else {
            line(interval, y, width - interval, y);
            noStroke();
            text(yphrase[ystep].toUpperCase(), 6, y + 6);
        }
        if (ystep < yphrase.length - 1) ystep++;
    }
}

function isVowel(c) {
    if (parseInt(c)) {
        return parseInt(c) % 2 !== 0;
    } else {
        c = c.toUpperCase();
        return "AEIOU".includes(c);
    }
}

